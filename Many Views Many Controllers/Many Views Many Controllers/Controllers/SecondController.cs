﻿using Microsoft.AspNetCore.Mvc;

namespace ManyViewsManyControllers.Controllers
{
    public class SecondController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}