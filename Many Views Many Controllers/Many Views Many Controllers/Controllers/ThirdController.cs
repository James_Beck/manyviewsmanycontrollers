﻿using Microsoft.AspNetCore.Mvc;

namespace ManyViewsManyControllers.Controllers
{
    public class ThirdController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}