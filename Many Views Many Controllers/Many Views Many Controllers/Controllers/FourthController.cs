﻿using Microsoft.AspNetCore.Mvc;

namespace ManyViewsManyControllers.Controllers
{
    public class FourthController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}